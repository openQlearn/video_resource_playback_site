/*
Navicat MySQL Data Transfer

Source Server         : 程华亮
Source Server Version : 50173
Source Host           : 65.49.199.20:3306
Source Database       : imp4m

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2018-05-07 09:50:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_catalog
-- ----------------------------
DROP TABLE IF EXISTS `t_catalog`;
CREATE TABLE `t_catalog` (
  `id` varchar(255) NOT NULL,
  `isUse` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `isVip` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_decade
-- ----------------------------
DROP TABLE IF EXISTS `t_decade`;
CREATE TABLE `t_decade` (
  `id` varchar(255) NOT NULL,
  `isUse` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sort` int(255) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_film
-- ----------------------------
DROP TABLE IF EXISTS `t_film`;
CREATE TABLE `t_film` (
  `id` varchar(255) NOT NULL,
  `actor` varchar(255) DEFAULT NULL,
  `cataLogName` varchar(255) DEFAULT NULL,
  `cataLog_id` varchar(255) DEFAULT NULL,
  `evaluation` double NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `isUse` int(11) NOT NULL,
  `locName` varchar(255) DEFAULT NULL,
  `loc_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `onDecade` varchar(255) DEFAULT NULL,
  `plot` text,
  `resolution` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `subClassName` varchar(255) DEFAULT NULL,
  `subClass_id` varchar(255) DEFAULT NULL,
  `typeName` varchar(255) DEFAULT NULL,
  `type_id` varchar(255) DEFAULT NULL,
  `updateTime` varchar(255) DEFAULT NULL,
  `isVip` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_level
-- ----------------------------
DROP TABLE IF EXISTS `t_level`;
CREATE TABLE `t_level` (
  `id` varchar(255) NOT NULL,
  `isUse` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_loc
-- ----------------------------
DROP TABLE IF EXISTS `t_loc`;
CREATE TABLE `t_loc` (
  `id` varchar(255) NOT NULL,
  `isUse` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_raty
-- ----------------------------
DROP TABLE IF EXISTS `t_raty`;
CREATE TABLE `t_raty` (
  `id` varchar(255) NOT NULL,
  `enTime` varchar(255) DEFAULT NULL,
  `film_id` varchar(255) DEFAULT NULL,
  `score` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_res
-- ----------------------------
DROP TABLE IF EXISTS `t_res`;
CREATE TABLE `t_res` (
  `id` varchar(255) NOT NULL,
  `episodes` int(11) NOT NULL,
  `isUse` int(11) NOT NULL,
  `link` text,
  `linkType` varchar(255) DEFAULT NULL,
  `name` text,
  `updateTime` varchar(255) DEFAULT NULL,
  `film_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_knc69bmssxu89jk9lmj736yhd` (`film_id`),
  CONSTRAINT `FK_knc69bmssxu89jk9lmj736yhd` FOREIGN KEY (`film_id`) REFERENCES `t_film` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_subclass
-- ----------------------------
DROP TABLE IF EXISTS `t_subclass`;
CREATE TABLE `t_subclass` (
  `id` varchar(255) NOT NULL,
  `isUse` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `catalog_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_dui4eygakshpow7ia82sf0wff` (`catalog_id`),
  CONSTRAINT `FK_dui4eygakshpow7ia82sf0wff` FOREIGN KEY (`catalog_id`) REFERENCES `t_catalog` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_type
-- ----------------------------
DROP TABLE IF EXISTS `t_type`;
CREATE TABLE `t_type` (
  `id` varchar(255) NOT NULL,
  `isUse` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `subclass_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_1bgyfw3vyq0tadn4wuyykopak` (`subclass_id`),
  CONSTRAINT `FK_1bgyfw3vyq0tadn4wuyykopak` FOREIGN KEY (`subclass_id`) REFERENCES `t_subclass` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` varchar(50) NOT NULL,
  `isVip` int(10) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `expireTime` datetime DEFAULT NULL,
  `userEmail` varchar(255) DEFAULT NULL,
  `userName` varchar(255) DEFAULT NULL,
  `userPasswd` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `t_user` (`id`, `isVip`, `createTime`, `expireTime`, `userEmail`, `userName`, `userPasswd`) VALUES ('ff80808163150c740163150da3100000', '1', '2018-04-30 01:36:45', '2020-12-01 01:36:45', 'xlosy14@163.com', 'xlosy', 'A6032E2212DF13A6A2BBF9D964C190CC');

-- ----------------------------
-- Table structure for t_vipcode
-- ----------------------------
DROP TABLE IF EXISTS `t_vipcode`;
CREATE TABLE `t_vipcode` (
  `id` varchar(225) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `is_use` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `expire_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
